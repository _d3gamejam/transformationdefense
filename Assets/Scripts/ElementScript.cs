﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementScript : MonoBehaviour
{
    public int objectValue;
    Animator Animator;

    void Start()
    {
        Animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform == transform)
                {
                    StartCoroutine(ButtonPressed());

                }
            }
                
        }
            
    }

    IEnumerator ButtonPressed()
    {
        Animator.Play("Button_press", 0, 0);
        yield return new WaitForSeconds(.2f);
        CannonController.instance.ElementSelected(objectValue);
    }

}
