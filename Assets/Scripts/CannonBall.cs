﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    public int CannonBallId;
    Rigidbody Body;
    EnemyScript _target;
    Sequence fireSequence;
    float speed = 10f;
    float lifeTime = 2f;
    private void Awake()
    {
        Body = GetComponent<Rigidbody>();
    }
    internal void SetTargetTransform(EnemyScript target)
    {
        if (target)
            _target = target;
        BallFired();
    }
    private void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
            Destroy(this.gameObject);
    }
    void BallFired()
    {
        if (_target)
        {
            float height = 1.5f;
            Vector3 dir = Vector3.Cross((_target.transform.position - Body.position).normalized, Vector3.right);
            Vector3 midPos = ((_target.transform.position - Body.position) / 2) + height * dir;
            //midPos.x = Body.position.x;

            fireSequence = DOTween.Sequence();
            Vector3 currentPos = Body.position;
            if (Vector3.Distance(currentPos, _target.transform.position) < 8f)
            {
                if (Vector3.Distance(currentPos, _target.transform.position) > 4.5f)
                    fireSequence.Append(DOTween.To(() => currentPos, x => currentPos = x, midPos, Vector3.Distance(currentPos, midPos) / speed).SetEase(Ease.Linear));
                fireSequence.Append(DOTween.To(() => currentPos, x => currentPos = x, (_target.transform.position + Vector3.up * .5f), Vector3.Distance(midPos, _target.transform.position) / speed).SetEase(Ease.Linear));

                fireSequence.OnUpdate(() =>
                {
                    Body.MovePosition(currentPos);
                });
                fireSequence.OnComplete(() =>
                {
                    Destroy(this.gameObject);
                });
            }
            else
            {
                Body.isKinematic = false;
                Body.velocity = Vector3.forward * speed;
            }

        }
        else
        {
            Body.isKinematic = false;
            Body.velocity = Vector3.forward * speed;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (_target && _target.IsAlive && CannonBallId == _target.EnemyId)
                _target.Kill(false);
            Destroy(this.gameObject);
        }
    }
    private void OnDestroy()
    {
        fireSequence?.Kill();
    }
    private void OnDrawGizmos()
    {
        if (_target)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(Body.position, .5f);
            Gizmos.DrawLine(Body.position, _target.transform.position);
            Vector3 dir = Vector3.Cross((_target.transform.position - Body.position).normalized, Vector3.right);
            Gizmos.DrawSphere(((_target.transform.position - Body.position) / 2) + 2 * dir, .2f);
            Gizmos.DrawSphere(_target.transform.position, .5f);
        }
    }
}
