﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager Instance { get; private set; }
    public GameObject[] EnemyPrefabs;
    public Transform SpawnPosition;
    public Transform BridgePosition;
    public Transform WallPosition;
    public int EnemyCount { get => SpawnedEnemies.Count; }

    private float xOffset = 6f;
    public float spawnInterval = 3f;


    private List<EnemyScript> SpawnedEnemies = new List<EnemyScript>();

    private void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        GameManager.instance.isGameActive = true;
        StartCoroutine(SpawnTarget());
    }


    void SpawnRandomEnemy()
    {
        int enemyIndex = Random.Range(0, EnemyPrefabs.Length);
        Vector3 spawnPos = new Vector3(Random.Range(SpawnPosition.position.x - xOffset, SpawnPosition.position.x + xOffset), SpawnPosition.position.y, SpawnPosition.position.z);

        GameObject TempEnemy = Instantiate(EnemyPrefabs[enemyIndex], spawnPos, EnemyPrefabs[enemyIndex].transform.rotation);
        EnemyScript enemyScript = TempEnemy.GetComponent<EnemyScript>();
        enemyScript.MoveTo(BridgePosition, WallPosition);

        SpawnedEnemies.Add(enemyScript);
    }

    public EnemyScript GetNeartesEnemy(Vector3 position)
    {
        if (SpawnedEnemies.Count > 0)
        {
            List<EnemyScript> alivedEnemies = SpawnedEnemies.Where(e => e.IsAlive).ToList();
            if (alivedEnemies.Count > 0)
                return alivedEnemies.OrderBy(e => Vector3.Distance(position, e.transform.position)).ToArray()[0];
        }

        return null;
    }

    internal void ClearAllEnemies()
    {
        if (SpawnedEnemies.Count > 0)
        {
            foreach (EnemyScript e in SpawnedEnemies)
                e.KillThemAll();
        }
    }

    public void EnemyCharacterDestroyed(EnemyScript destroyedEnemy)
    {
        if (SpawnedEnemies.Contains(destroyedEnemy))
            SpawnedEnemies.Remove(destroyedEnemy);
    }

    public IEnumerator SpawnTarget()
    {
        while (GameManager.instance.isGameActive)
        {
            SpawnRandomEnemy();
            yield return new WaitForSeconds(spawnInterval);

        }
    }


}
