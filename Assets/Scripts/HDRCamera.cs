﻿using UnityEngine;

[ExecuteInEditMode]
public class HDRCamera : MonoBehaviour
{
    private void OnEnable()
    {
        Camera.main.allowHDR = true;
    }
}
