﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundTypes
{
    CannonShot,
    EnemyExplode,
    CannonChange,
    JellyWalk,
    CannonTransform
}
public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance { get; private set; }
    [SerializeField] private List<AudioClip> Sounds;
    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this);
    }
    public AudioClip GetSound(SoundTypes soundType)
    {
        return Sounds[(int)soundType];
    }
}
