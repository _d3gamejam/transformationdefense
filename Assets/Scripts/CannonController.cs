﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CannonController : MonoBehaviour
{
    private GameObject CannonBall;
    public GameObject[] CannonBallPrefab;
    public float RotationSpeed;
    private float CannonHealth = 3;
    float CannonHealthValue = 100;
    public Transform BallSpawnPoint;
    public MeshRenderer[] ColoredOutsideMaterials;
    public MeshRenderer[] ColoredInsideMaterials;
    public Material green;
    public Material greenInside;
    public Material blue;
    public Material blueInside;
    public Material red;
    public Material redInside;
    public RectTransform HealthBar;
    public TextMeshPro HealtText;
    public MeshRenderer CannonBody;

    Animator Animator;
    AudioSource AudioSource;

    float fireTimer = 0f;
    float fireTimeValue = .5f;

    public static CannonController instance { get; private set; }
    Color currentBodyColor;
    Vector3 initialLocalPos;

    private void Awake()
    {
        instance = this;
        Animator = GetComponent<Animator>();
        AudioSource = GetComponent<AudioSource>();
        currentBodyColor = CannonBody.material.color;
        initialLocalPos = transform.localPosition;
    }

    void Start()
    {
        fireTimer = fireTimeValue;
        //nextObjectValue = 2; // Water:0   Fire:1   Wind:2
        ColorChange();
        isInitialSound = false;
        SetHealthBar(1f);
        HealtText.text = ((int)CannonHealthValue).ToString();
    }

    void Update()
    {

        //float horizontalInput = Input.GetAxis("Horizontal");

        //transform.Rotate(Vector3.up, horizontalInput * RotationSpeed * Time.deltaTime);
        fireTimer += Time.deltaTime;

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform == transform)
                {
                    Fire();
                }
            }
        }


    }
    void Fire()
    {
        if (fireTimer >= fireTimeValue && !colorChanging)
        {
            // Launch a projectile from the player
            GameObject cannonBall = Instantiate(CannonBall, BallSpawnPoint.position, transform.rotation);
            cannonBall.transform.position = BallSpawnPoint.position;
            EnemyScript nearestEnemy = SpawnManager.Instance.GetNeartesEnemy(transform.position);
            if (nearestEnemy && Vector3.Distance(nearestEnemy.transform.position, transform.position) > 4.5f)
                transform.DORotateQuaternion(Quaternion.LookRotation(nearestEnemy.transform.position - transform.position), .25f).SetEase(Ease.Linear);
            cannonBall.GetComponent<CannonBall>().SetTargetTransform(nearestEnemy);

            AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.CannonShot);
            AudioSource.Play();
            Animator.Play("Cannon_shoot", 0, 0);
            fireTimer = 0;
        }
    }

    Tween hpAnim;
    public void CannonIsHit()
    {
        if (CannonHealth > 0)
        {
            CannonHealth--;
            SetDamage();
            BlobExplodeSequence();
            hpAnim?.Kill();
            hpAnim = DOTween.To(() => CannonHealthValue, x => CannonHealthValue = x, Mathf.Lerp(0, 100, Mathf.InverseLerp(0, 3, CannonHealth)), .5f).OnUpdate(() =>
            {
                SetHealthBar(Mathf.InverseLerp(0, 100, CannonHealthValue));
                HealtText.text = ((int)CannonHealthValue).ToString();

                if (((int)CannonHealthValue) <= 1)
                {
                    Debug.Log("Cannon is destroyed, Game is over.");
                    //Destroy(GameObject.Find("Cannon"));
                    StartCoroutine(CannonDeathAnim());
                    GameManager.instance.GameOver();
                }
            });
        }
    }
    Sequence colorSequence;
    void SetDamage()
    {
        colorSequence?.Kill();
        colorSequence = DOTween.Sequence();
        colorSequence.Append(CannonBody.material.DOColor(new Color(0.6235294f, 0.117647f, 0.1490196f), .3f));
        colorSequence.Append(CannonBody.material.DOColor(currentBodyColor, .3f));

    }
    Sequence zPosSequence;
    void BlobExplodeSequence()
    {
        zPosSequence?.Kill();
        zPosSequence = DOTween.Sequence();
        zPosSequence.Append(transform.DOLocalMoveZ(-0.4f, .1f).SetEase(Ease.OutCubic));
        zPosSequence.Append(transform.DOLocalMoveZ(initialLocalPos.z, .1f).SetEase(Ease.OutCubic));
    }
    IEnumerator CannonDeathAnim()
    {
        CameraShake.instance.shakeDuration = 0.4f;
        Animator.Play("Cannon_death", 0, 0);
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(Animator.GetCurrentAnimatorStateInfo(0).length);
        yield return new WaitUntil(() => SpawnManager.Instance.EnemyCount <= 0);
        GameManager.instance.RestartGame();
    }
    void SetHealthBar(float value)
    {
        HealthBar.localRotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Lerp(-119, -12, value)));
    }

    bool isInitialSound = true;
    public void ColorChange()
    {
        if (!isInitialSound)
        {
            AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.CannonTransform);
            AudioSource.Play();
        }

        if (nextObjectValue == 0) //blue (35p)
        {
            CannonBall = CannonBallPrefab[0];
            foreach (MeshRenderer mR in ColoredOutsideMaterials)
            {
                mR.sharedMaterial = blue;
            }
            foreach (MeshRenderer mR in ColoredInsideMaterials)
            {
                mR.sharedMaterial = blueInside;
            }
        }

        else if (nextObjectValue == 1) //red (13p)
        {
            CannonBall = CannonBallPrefab[1];
            foreach (MeshRenderer mR in ColoredOutsideMaterials)
            {
                mR.sharedMaterial = red;
            }
            foreach (MeshRenderer mR in ColoredInsideMaterials)
            {
                mR.sharedMaterial = redInside;
            }

        }

        else if (nextObjectValue == 2) //green (20p)
        {
            CannonBall = CannonBallPrefab[2];
            foreach (MeshRenderer mR in ColoredOutsideMaterials)
            {
                mR.sharedMaterial = green;
            }
            foreach (MeshRenderer mR in ColoredInsideMaterials)
            {
                mR.sharedMaterial = greenInside;
            }

        }
    }



    int nextObjectValue
    {
        get { return PlayerPrefs.GetInt("nextObjectValue", 2); }
        set { PlayerPrefs.SetInt("nextObjectValue", value); }
    }
    bool colorChanging = false;
    internal void ElementSelected(int objectValue)
    {
        if (!colorChanging)
        {
            Debug.Log("Object Value:" + objectValue);
            var cubeRenderer = GetComponent<Renderer>();
            nextObjectValue = objectValue;
            StartCoroutine(ColorChangeAnim());
            AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.CannonChange);
            AudioSource.Play();
        } 
    }
    IEnumerator ColorChangeAnim()
    {
        colorChanging = true;
        Animator.Play("Cannon_transform", 0, 0);
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(Animator.GetCurrentAnimatorStateInfo(0).length);
        colorChanging = false;
    }







}



